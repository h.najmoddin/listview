package ir.co.ariapardaz.listviewdefult;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        foodmodel f1 = new foodmodel();
        f1.name = "piza";
        f1.category = "fastfood";
        f1.callery = 200;
        f1.price = 15000;


        foodmodel f2 = new foodmodel();
        f2.name = "polo";
        f2.category = "irani";
        f2.callery = 80;
        f2.price = 8000;

        foodmodel f3 = new foodmodel();
        f3.name = "neskafe";
        f3.category = "cafe";
        f3.callery = 160;
        f3.price = 3500;


        final List<foodmodel> foodmodels = new ArrayList<foodmodel>();
        foodmodels.add(f1);
        foodmodels.add(f2);
        foodmodels.add(f3);


        ListView listView = (ListView) findViewById(R.id.list_item);
        Adapter adapter = new Adapter(MainActivity.this, foodmodels);
        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent detal = new Intent(MainActivity.this, Viewdetail.class);

                detal.putExtra("name", foodmodels.get(position).getName());
                startActivity(detal);

            }
        });


    }
}
