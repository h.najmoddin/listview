package ir.co.ariapardaz.listviewdefult;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.security.acl.Group;
import java.util.List;

/**
 * Created by m on 2019/06/04.
 */

public class Adapter extends BaseAdapter {
    private Context context;
    private List<foodmodel> foodmodels;

    public Adapter() {
    }

    public Adapter(Context context, List<foodmodel> foodmodels) {
        this.context = context;
        this.foodmodels = foodmodels;
    }

    @Override
    public int getCount() {
        return foodmodels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View viewrow = LayoutInflater.from(context).inflate(R.layout.food_item, parent, false);
        TextView name=(TextView)viewrow.findViewById(R.id.Name);
        TextView category=(TextView)viewrow.findViewById(R.id.category);
        TextView price=(TextView)viewrow.findViewById(R.id.price);
        TextView collory=(TextView)viewrow.findViewById(R.id.callory );

        name.setText(foodmodels.get(position).getName());
        category.setText(foodmodels.get(position).getCategory());
        price.setText(foodmodels.get(position).getPrice()+"");
        collory.setText(foodmodels.get(position).getCallery()+"");



        return viewrow;
    }
}
