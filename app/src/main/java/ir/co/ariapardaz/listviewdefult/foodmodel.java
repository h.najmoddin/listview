package ir.co.ariapardaz.listviewdefult;

/**
 * Created by m on 2019/06/04.
 */

public class foodmodel {



String name;
String category;
int price,callery;

    public foodmodel(String name, String category, int price, int callery) {
        this.name = name;
        this.category = category;
        this.price = price;
        this.callery = callery;
    }

    public foodmodel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCallery() {
        return callery;
    }

    public void setCallery(int callery) {
        this.callery = callery;
    }

    @Override
    public String toString() {
        return "foodmodel{" +
                "name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", price=" + price +
                ", callery=" + callery +
                '}';
    }
}
